﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_AIController : MonoBehaviour {

    public Transform[] waypoints;
    private Transform tf;
    public Test_Data tData;
    public Test_Motor tMotor;

    private int currentWaypoint;
    public float closeEnough;


    public enum LoopType{ Stop, Loop, PingPong};
    public LoopType loopType;

    private bool isPatrollingForward;

    private void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start () {
        isPatrollingForward = true;
	}
	
	// Update is called once per frame
	void Update () {

        //TODO: Make our loops into Switch/Case sections as well as Functions. Clean this practice code up.
        if (RotateTowards(waypoints[currentWaypoint].position, tData.testRotateSpeed))
        {
            //Do Nothing...
        }
        else
        {
            //Move Forward
            tMotor.TestMove(tData.testMoveSpeed);
        }

        //If we start to get close to the waypoint
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            if (loopType == LoopType.Stop)
            {
                //Move out to next waypoint
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
            }
            else if (loopType == LoopType.Loop)
            {
                //Move out to next waypoint
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }
            }
            else if (loopType == LoopType.PingPong)
            {
                if (isPatrollingForward)
                {
                    //Move out to next waypoint
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        //Reverse our direction and decrement out waypoints.
                        isPatrollingForward = false;
                        currentWaypoint--;
                    }
                }
                else
                {
                    if (currentWaypoint > 0)
                    {
                        currentWaypoint--;
                    }
                    else
                    {
                        isPatrollingForward = true;
                        currentWaypoint++;
                    }
                }
            }
        }
	}

    //RotateTowards(Target, speed) -- This function will rotate towards the target if possible.
    //If we rotate, return true. If unable to rotate due to the fact we are facing the object then return false.

    public bool RotateTowards(Vector3 target, float speed)
    {
        //TODO: When creating this in a non-test format. Clean and Organize. Optimize.
        Vector3 vectorToTarget;

        //The vector to our target is the difference between the target position and our current position.
        //How would our position need to be different to reach the target? Subtraction, that's how.
        vectorToTarget = target - tf.position;

        //Find the Quaternion that looks down our vector.
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
        if (targetRotation == tf.rotation)
        {
            return false;
        }
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, tData.testRotateSpeed * Time.deltaTime);
        return true;
    }

}
