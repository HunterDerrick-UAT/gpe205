﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample_AIController : MonoBehaviour {

    public enum AttackMode{Chase, Flee};
    public AttackMode attackMode;
    public Transform target;
    public Transform tf;
    public Test_Motor tMotor;
    public Test_Data tData;

    [Range(1.0f,10.0f)]
    public float fleeDistance;


	// Use this for initialization
	void Start () {
        //tf = gameObject.GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
        if (attackMode == AttackMode.Chase)
        {
            //Move Forward
            tMotor.TestMove(tData.testMoveSpeed);
            //Rotate Toward Target
            tMotor.RotateTowards(target.position, tData.testRotateSpeed);
        }

        if (attackMode == AttackMode.Flee)
        {
            //The Vector from ai to target(Player) is target(player) position minus AI position. 
            Vector3 vectorToTarget = target.position - tf.position;

            //We can flip this vector by -1 to get a vector away from our target. 
            Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

            //Now we can normalize the vector to give it a magnitude of 1
            vectorAwayFromTarget.Normalize();

            //A normalized vector can be multiplied by a length to make a vector of that length. i.e if a normalized vector is 2(meters) multiply it by 2 and get a vector of 4 meters.
            vectorAwayFromTarget *= fleeDistance;

            //We can find the position in space we want to move to by adding our vector away from our AI to our AI's position.
            //This gives us a point that is "That vector away" from our current position.
            Vector3 fleePosition = vectorAwayFromTarget + tf.position;
            tMotor.RotateTowards(fleePosition, tData.testRotateSpeed);
            tMotor.TestMove(tData.testMoveSpeed);
            
        }
	}



}
