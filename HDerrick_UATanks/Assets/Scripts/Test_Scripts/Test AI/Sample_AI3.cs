﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample_AI3 : MonoBehaviour {

    
    private Transform target;
    private Transform tf;
    private Test_Motor tMotor;
    private Test_Data tData;
    private int avoidanceStage = 0;
    public float avoidanceTime;
    private float exitTime;

    public enum AttackMode{Chase};
    public AttackMode attackMode;


	// Use this for initialization
	void Start ()
    {
        tf = gameObject.GetComponent<Transform>(); //Attaches the transform of GameObject script is attached to.
        tMotor = gameObject.GetComponent<Test_Motor>();  //Attaches the Test_Motor script in the Hierarchy
        tData = gameObject.GetComponent<Test_Data>();  //Attaches the Test_Data script in the Hierarchy
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>(); //Attaches the GameObject of the target. In this case the player in the Hierarchy.
	}
	
	// Update is called once per frame
	void Update ()
    {
        //TODO: Make this cleaner.
        if (attackMode == AttackMode.Chase)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
                Debug.Log(avoidanceStage);
            }

            if (avoidanceStage == 0)
            {
                DoChase();
            }
        }
	}


    void DoChase()
    {
        //TODO: Make this cleaner
        tMotor.RotateTowards(target.position, tData.testRotateSpeed);
        //Check if we can move "tData.TestMoveSpeed" units away.
        //We are choosing this distance because that is how far we move in "1 second".
        //This means we are looking for collision. "1 Second in the future."
        if (CanMove(tData.testMoveSpeed))
        {
            tMotor.TestMove(tData.testMoveSpeed);
        }
        else
        {
            //Enter into Obstacle Avoidance Stage #1
            avoidanceStage = 1;
        }
    }

    void DoAvoidance()
    {
        //TODO: Make this
        if (avoidanceStage == 1)
        {
            //Rotate to the left
            tMotor.TestRotate(-1 * tData.testRotateSpeed);
            //If I can now move forward, than move to stage 2.
            if (CanMove(tData.testMoveSpeed))
            {
                avoidanceStage = 2;

                //Set number of seconds we will be in the avoidance stage.
                exitTime = avoidanceTime;
            }
            //Otherwise we will do this again next turn.
        }
        else if (avoidanceStage == 2)
        {
            //If we can move forward, do so. 
            if (CanMove(tData.testMoveSpeed))
            {
                //Subtract from our timer and move.
                exitTime -= Time.deltaTime;
                tMotor.TestMove(tData.testMoveSpeed);

                //If we have moved long enough, return to chase mode.
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                //Otherwise we can not move forward, so return to avoidanceStage 1
                avoidanceStage = 1;
            }
        }
    }

    bool CanMove(float speed)
    {
        //TODO: Make this. This will check if we can move forward by "Speed" units.

        //Cast a Ray forward in the distance/direction we sent in.
        //If our raycast hit something
        RaycastHit hit;
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            //If what we hit IS NOT THE player...
            if (!hit.collider.CompareTag("Player"))
            {
                //Then we can not move.
                return false;
            }
        }
        //Otherwise, return true.
        return true;
        
    }


}
