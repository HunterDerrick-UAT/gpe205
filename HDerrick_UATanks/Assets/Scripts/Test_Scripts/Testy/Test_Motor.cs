﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Motor : MonoBehaviour {

    private CharacterController testCharController; // This variable will hold our CharacterController component. 
    private Transform tf;
    public AI_Data aiData;

	// Use this for initialization
	void Start () {
        testCharController = gameObject.GetComponent<CharacterController>(); //Storing the CharacterController component onto the variable.
        tf = gameObject.GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {

	}

    //TestMove(): This is a test function. Purpose: Create a simple movement based system moving forwards/backwards on Z-Axis
    public void TestMove(float speed)
    {
        //Create a Private Vector3 to hold our Data for Speed
        Vector3 testSpeed;

        //Be sure to start the vector pointing in the same direction the GameObject is facing. In this case Z-Axis. Utilize transform.forward.
        testSpeed = transform.forward;

        //Currently the Vector is only 1 unit in length. We need to apply our float of name "speed".
        testSpeed *= speed;

        //Now we need to call the SimpleMove(); function from our CharacterController. The SimpleMove() will automatically apply Time.deltaTime and convert meters to seconds.
        testCharController.SimpleMove(testSpeed);
    }

    //TestRotate(): This is a test function. Purpose: Create a simple rotational based system that will rotate the GameObject left/right on the Y-Axis
    public void TestRotate(float speed)
    {
        //Create a private Vector3 to hold data for rotationSpeed
        Vector3 testRotation;

        //Be sure to start by rotating the vector by one degree per frame draw. Left is EQUAL to negative right. || Left = -1 Right = 1
        testRotation = Vector3.up;

        //Adjust our rotation based on the float "Speed"
        testRotation *= speed;

        //Change our rotation from per frame to per second. 
        testRotation *= Time.deltaTime;

        //Rotate the GameObject. HOWEVER: Rotate it in local space...not world space.
        transform.Rotate(testRotation, Space.Self);
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        //TODO: When creating this in a non-test format. Clean and Organize. Optimize.
        Vector3 vectorToTarget;

        //The vector to our target is the difference between the target position and our current position.
        //How would our position need to be different to reach the target? Subtraction, that's how.
        vectorToTarget = target - tf.position;

        //Find the Quaternion that looks down our vector.
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
        if (targetRotation == tf.rotation)
        {
            return false;
        }
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, aiData.aiRotateSpeed * Time.deltaTime);
        return true;
    }
}
