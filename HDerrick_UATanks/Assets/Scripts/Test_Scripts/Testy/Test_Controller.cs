﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Controller : MonoBehaviour {

    public Test_Motor tMotor; //Bringing in and allowing access to the Test_Motor script.
    public Test_Data tData;   //Bringing in and allowing access to the Test_Data script.

    public enum InputScheme {WASD, arrowKeys};    //Creating an ENUM for players/designers to select which buttons to move character with.
    public InputScheme input = InputScheme.WASD;  //Creating a base input scheme.

    public GameObject missileSpawn;  //GameObject used as placeholder where the Missile will spawn from. Drag from Heirarchy into inspector
    public GameObject missile;       //Used as placeholder for Missile. Drag from Prefabs, into inspector
    [Range(100.0f,5000.0f)]          //Establishing a range amount that can be adjusted for missileForce.
    public float missileForce;       //Float to adjust the amount of force of which the missle fires at. Adjust in the inspector.
    [Range(0.5f, 5.0f)]              //Establishing a range amount for destroying the missile.
    public float destroyMissile;     //Timer set to destroy missile.

    public float coolDown;           //Establish a set Cool Down. This will allow the player to shoot, only once the cool down reset. i.e: 1 or 2
    public float coolDownTimer;      //Making a float to perform a calculation to decrament time for the cool down.

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        PlayerControls();

        //If player presses down the SpaceBar. PlayerShoot() will activate and shoot a missile from the tank. Player will only be able to fire at a certain rate.
        CoolDown();

        //If our cool down has been reset, then the play can fire.
        if (Input.GetKeyDown(KeyCode.Space) && coolDownTimer == 0)
        {
            Debug.Log("Fire");
            PlayerShoot();
            coolDownTimer = coolDown;
        }
        
	}

    //Function PlayerControls(). Purpose: Based off of enum and switch. Allows desginer/player to choose which buttons to play/move character with.
    void PlayerControls()
    {
        switch (input)
        {
            case InputScheme.WASD:

                if (Input.GetKey(KeyCode.W))
                {
                    tMotor.TestMove(tData.testMoveSpeed);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    tMotor.TestMove(-tData.testMoveSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    tMotor.TestRotate(tData.testRotateSpeed);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    tMotor.TestRotate(-tData.testRotateSpeed);
                }
                break;
        }

        switch (input)
        {
            case InputScheme.arrowKeys:

                if (Input.GetKey(KeyCode.UpArrow))
                {
                    tMotor.TestMove(tData.testMoveSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    tMotor.TestMove(-tData.testMoveSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    tMotor.TestRotate(tData.testRotateSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    tMotor.TestRotate(-tData.testRotateSpeed);
                }
                break;
        }
    }

    //Function PlayerShoot(). Purpose: Instantiate a Missile that will fire from a selected spawn point located on the Tank. Spawn point label: MissileSpawn
    void PlayerShoot()
    {
        //Here we instantiate our missile
        GameObject temporaryMissile;
        temporaryMissile = Instantiate(missile, missileSpawn.transform.position, missileSpawn.transform.rotation) as GameObject;

        //Retreive our Rigidbody
        Rigidbody rb;
        rb = temporaryMissile.GetComponent<Rigidbody>();

        //Using the Rigidbody component, we will "AddForce" to our missile to move it forward.
        rb.AddForce(transform.forward * missileForce);

        //Destroy Missile after seconds. Designated in inspector.
        Destroy(temporaryMissile, destroyMissile);
    }

    //Function CoolDown(). Purpose: Set cooldown time for time in between shots. So player can not fire at a constant rate.
    public void CoolDown()
    {
        //If our CooldownTimer is greater than 0. Than we are subtracting time in this case seconds.
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        //Make sure that our Cool Down Timer never goes any lower than 0.
        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }
    }

}
