﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This will collect and hold data:
//Speed, RotationSpeed, Health, Shields, AmmoCount, etc..
public class Test_Data : MonoBehaviour {

    [Range(5, 15)]
    public float testMoveSpeed;  //In meters per second
    [Range(70, 220)]
    public float testRotateSpeed; //In degrees per second

    [Range(100.0f, 5000.0f)]          //Establishing a range amount that can be adjusted for missileForce.
    public float fireRate;       //Float to adjust the amount of force of which the missle fires at. Adjust in the inspector.

    public float testAiHealth;
    public float testAiMaxHealth = 100.0f;
    public float testPlayerHealth;
    public float testPlayerMaxHealth = 200.0f;

    public GameObject ai;
    public GameObject player;



    public void RemoveAiHealth(float amount)
    {
        testAiHealth -= amount;
        if (testAiHealth <= 0)
        {
            Destroy(ai);
        }
    }

    public void RemovePlayerHealth(float amount)
    {
        testPlayerHealth -= amount;
        if (testPlayerHealth <= 0)
        {
            Destroy(player);
        }
    }


}
