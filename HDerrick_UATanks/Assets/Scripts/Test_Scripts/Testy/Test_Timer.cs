﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Timer : MonoBehaviour {

    public float nextTimerDelay; //Float allowed to be edited in inspector for choosing a delay timer.
    private float nextEventTime; //Private float that will be utilized to check when the next event is supposed to happen.

    public float lastTimerDelay = 1; //Float that is checking for time alloted for last event to occur.
    private float lastEventTime; //Private float that will be utilized to check when the last event is supposed to occur.

    public float untilTimerDelay = 1;
    private float timeUntilNextEvent;

    
	// Use this for initialization
	void Start ()
    {
        nextEventTime = Time.time + nextTimerDelay; //The next time this event can occur is "timerDelay" amount of seconds after right now.
        lastEventTime = Time.time;
        timeUntilNextEvent = untilTimerDelay;
	}
	
	// Update is called once per frame
	void Update () {
        //Testing basic movement per frame draw on the Z-Axis
        //transform.position += transform.forward * Time.deltaTime *2f;

        float myVector1 = 3.0f; // A one-dimensional vector is just a float
        Debug.Log(Mathf.Abs(myVector1)); // Output its magnitude
        Debug.Log(Mathf.Sign(myVector1)); // Output its direction (1 or -1)

        Vector2 myVector = new Vector2(2, -2); // Create a vector 2 units on x, and -2 units on y
        Debug.Log(myVector.magnitude + " Vector2"); // Output its magnitude

        Vector3 myVector3 = new Vector3(2, -2, 27); // Create a vector 2 units on x, -2 units on y, and 27 on z
        Debug.Log(myVector3.magnitude + " Vector3"); // Output its magnitude

        //if (Time.time >= nextEventTime)
        //{
        //    Debug.Log("The 'NEXT' event.");
        //    nextEventTime = Time.time + nextTimerDelay;
        //}

        //if (Time.time >= lastEventTime + lastTimerDelay)
        //{
        //    Debug.Log("This is the LAST event.");
        //    lastEventTime = Time.time;
        //}

        ////Comparing the time remaining in a frame draw and subtracting. 
        //timeUntilNextEvent -= Time.deltaTime;
        //if (timeUntilNextEvent <= 0)
        //{
        //    Debug.Log("UNTIL next event.");
        //    timeUntilNextEvent = untilTimerDelay;
        //}


	}
}
