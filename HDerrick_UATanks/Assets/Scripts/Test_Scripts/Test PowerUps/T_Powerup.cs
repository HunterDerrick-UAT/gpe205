﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class T_Powerup {


    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float fireRateModifier;

    public float duration;

    public bool isPermanent;


    public void OnActivate(Test_Data target)
    {
        target.testMoveSpeed += speedModifier;
        target.testPlayerHealth += healthModifier;
        target.testPlayerMaxHealth += maxHealthModifier;
        target.fireRate += fireRateModifier;
    }

    public void OnDeactivate(Test_Data target)
    {
        target.testMoveSpeed -= speedModifier;
        target.testPlayerHealth -= healthModifier;
        target.testPlayerMaxHealth -= maxHealthModifier;
        target.fireRate -= fireRateModifier;
    }

}
