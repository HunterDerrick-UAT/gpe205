﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class T_CheatController : MonoBehaviour {


    public T_PowerupController powerController;
    public T_Powerup cheatPowerup;

	// Use this for initialization
	void Start () {
        if (powerController == null)
        {
            powerController = gameObject.GetComponent<T_PowerupController>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        HueCheatCode();
	}

    public void HueCheatCode()
    {
        //If H and U are being held down and this is the first frame the "e" is pressed...do something.
        if (Input.GetKey(KeyCode.H) && Input.GetKey(KeyCode.U) && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("So, this is what cheating feels like?");
            //Add a powerup to our tank.
            powerController.Add(cheatPowerup);
        }
    }
}
