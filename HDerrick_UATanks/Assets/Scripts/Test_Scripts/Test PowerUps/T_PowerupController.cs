﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class T_PowerupController : MonoBehaviour {

    public Test_Data tData;
    public List<T_Powerup> powerups;

	// Use this for initialization
	void Start () {
        powerups = new List<T_Powerup>();
        tData = gameObject.GetComponent<Test_Data>();
    }
	
	// Update is called once per frame
	void Update () {
        CheckForPowerUps();
	}


    public void Add(T_Powerup powerup)
    {
        //Run the OnActivate function of the powerup
        powerup.OnActivate(tData);

        //Only add the permanent ones to the list.
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
        
    }


    public void CheckForPowerUps()
    {
        //Create a list o hold our expired power ups
        List<T_Powerup> expiredPowerups = new List<T_Powerup>();

        //Loop through all the powers in the list
        foreach (T_Powerup power in powerups)
        {
            //Subtract from the timer.
            power.duration -= Time.deltaTime;

            //Assemble a list of expired powerups.
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }

        //Now that we have looked through every powerup in our list, we use our expired power up list to remove the expired ones.
        foreach (T_Powerup power in expiredPowerups)
        {
            power.OnDeactivate(tData);
            powerups.Remove(power);
        }

        //Our expiredPowerups are stored locally and will just "poof" into nothing when the function ends.
        //Let's clear it to learn how to empty a list.
        expiredPowerups.Clear();
                          
    }

}
