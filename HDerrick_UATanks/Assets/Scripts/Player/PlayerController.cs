﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private PlayerMotor pMotor; //Bringing in and allowing access to the Test_Motor script.
    private PlayerData pData;   //Bringing in and allowing access to the Test_Data script..
    
    public enum InputScheme { WASD, arrowKeys };    //Creating an ENUM for players/designers to select which buttons to move character with.
    public InputScheme input = InputScheme.WASD;  //Creating a base input scheme.

    public GameObject missileSpawn;  //GameObject used as placeholder where the Missile will spawn from. Drag from Heirarchy into inspector
    public GameObject missile;       //Used as placeholder for Missile. Drag from Prefabs, into inspector

    [Range(10.0f,100.0f)]
    public float missileDamage;      //Float to set missileDamage.

    [Range(0.5f, 5.0f)]              //Establishing a range amount for destroying the missile.
    public float destroyMissile;     //Timer set to destroy missile.

    public float coolDown;           //Establish a set Cool Down. This will allow the player to shoot, only once the cool down reset. i.e: 1 or 2
    public float coolDownTimer;      //Making a float to perform a calculation to decrament time for the cool down.

    public AudioClip playerShooting;

    // Use this for initialization
    void Start()
    {
        if (pData == null)
        {
            //Debug.Log("Accessing the Player Data on the Player Controller");
            pData = gameObject.GetComponent<PlayerData>();
        }
        if (pMotor == null)
        {
            //Debug.Log("Accessing the Player Motor on the Player Controller");
            pMotor = gameObject.GetComponent<PlayerMotor>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawRay(transform.position, transform.forward, Color.green * 50);
        PlayerControls();
        //If player presses down the SpaceBar. PlayerShoot() will activate and shoot a missile from the tank. Player will only be able to fire at a certain rate.
        CoolDown();
        
    }

    //Function PlayerControls(). Purpose: Based off of enum and switch. Allows desginer/player to choose which buttons to play/move character with.
    void PlayerControls()
    {
        switch (input)
        {
            case InputScheme.WASD:

                if (Input.GetKey(KeyCode.W))
                {
                    pMotor.PlayerMove(pData.MoveSpeed);
                }
                if (Input.GetKey(KeyCode.S))
                {
                   pMotor.PlayerMove(-pData.MoveSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    pMotor.PlayerRotate(pData.RotateSpeed);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    pMotor.PlayerRotate(-pData.RotateSpeed);
                }
                if (Input.GetKeyDown(KeyCode.Space) && coolDownTimer == 0)
                {
                    Debug.Log("Player One Firing");
                    AudioSource.PlayClipAtPoint(playerShooting, transform.position, 1.0f);
                    PlayerShoot();
                    coolDownTimer = coolDown;
                }
                break;
        }

        switch (input)
        {
            case InputScheme.arrowKeys:

                if (Input.GetKey(KeyCode.UpArrow))
                {
                    pMotor.PlayerMove(pData.MoveSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    pMotor.PlayerMove(-pData.MoveSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    pMotor.PlayerRotate(pData.RotateSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    pMotor.PlayerRotate(-pData.RotateSpeed);
                }
                if (Input.GetKeyDown(KeyCode.RightShift) && coolDownTimer == 0)
                {
                    Debug.Log("Player Two Firing");
                    PlayerShoot();
                    coolDownTimer = coolDown;
                }
                break;
        }
    }

    //Function PlayerShoot(). Purpose: Instantiate a Missile that will fire from a selected spawn point located on the Tank. Spawn point label: MissileSpawn
    void PlayerShoot()
    {

        
        //Here we instantiate our missile
        GameObject temporaryMissile;
        temporaryMissile = Instantiate(missile, missileSpawn.transform.position, missileSpawn.transform.rotation) as GameObject;

        //Retreive our Rigidbody
        Rigidbody rb;
        rb = temporaryMissile.GetComponent<Rigidbody>();

        //Using the Rigidbody component, we will "AddForce" to our missile to move it forward.
        rb.AddForce(transform.forward * pData.missileForce);

        //Destroy Missile after seconds. Designated in inspector.
        Destroy(temporaryMissile, destroyMissile);

        
    }

    //Function CoolDown(). Purpose: Set cooldown time for time in between shots. So player can not fire at a constant rate.
    public void CoolDown()
    {
        //If our CooldownTimer is greater than 0. Than we are subtracting time in this case seconds.
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        //Make sure that our Cool Down Timer never goes any lower than 0.
        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }
    }
}
