﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {

    [Range(5, 15)]
    public float MoveSpeed;  //In meters per second
    [Range(70, 220)]
    public float RotateSpeed; //In degrees per second
    public float missileForce;  //Set the fire rate of Tank
    [SerializeField]
    private GameObject player;

    

}
