﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class MapManager : MonoBehaviour {

    public int rows;
    public int columns;
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;
    public GameObject[] gridPrefabs;
    private Room[,] grid;
    public int mapSeed;
    public bool isMapOfTheDay;


    private void Awake()
    {
        //Clear out the grid - "Which Column" is X and "Which Row" is Y.
        grid = new Room[columns, rows];
    }

    private void Start()
    {
        GameManager.instance.SpawnPlayer();

        GenerateGrid();
        if (isMapOfTheDay)
        {
            mapSeed = DateToInt(DateTime.Now.Date);
        }
    }

    public void GenerateGrid()
    {

        UnityEngine.Random.seed = DateToInt(DateTime.Now);

        //For each grid ROW
        for (int r = 0; r < rows; r++)
        {
            //For each COLUMN in that ROW.
            for (int c = 0; c < columns; c++)
            {
                //Find the location.
                float xPosition = roomWidth * c;
                float zPosition = roomHeight * r;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                //Create a new grid at the correct location.
                GameObject temporaryRoom = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;

                //Set the Grids Parent.
                temporaryRoom.transform.parent = this.transform;

                //Give it a nice name.
                temporaryRoom.name = "Room_" + c + "," + r;

                //Get the Room Object.
                Room tempRoom = temporaryRoom.GetComponent<Room>();

                //OPEN THE DOORS - NORTH AND SOUTH
                //If we are on the bottom row, then open the NORTH Door.
                if (r == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (r == rows - 1)
                {
                    //Otherwise, if we are on the top row. Open the SOUTH Door.
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    //Otherwise, we are located in the MIDDLE. So open BOTH North and South Doors.
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }

                //OPEN THE DOORS - EAST AND WEST
                //If we are on the first COLUMN, open the EAST Door.
                if (c == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (c == columns - 1)
                {
                    //Otherwise, if we are on the LAST COLUMN. Open the WEST Door.
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    //Otherwise, we are located in the MIDDLE. So open BOTH East and West Doors.
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }

                //Save this grid to an ARRAY.
                grid[c, r] = tempRoom;
            }
        }
    }

    //Returns a random room
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    public int DateToInt(DateTime dateToUse)
    {
        //Add our date and return it.
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

}
