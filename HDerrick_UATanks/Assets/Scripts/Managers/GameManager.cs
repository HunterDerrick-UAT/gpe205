﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    //[SerializeField]
    public GameObject playerPrefab;
    //[SerializeField]
    public GameObject player;
    [SerializeField]
    private GameObject ai;
    [SerializeField]
    private GameObject aiPrefab;
    private AI_Data aiData;
    [SerializeField]
    private Transform playerSpawnLocation;
    [SerializeField]
    private Transform[] aiSpawnLocation;

    public bool aiDied;
    public bool playerDied;
    public bool playerWinTrigger;

    public int playerLives;
    public int aiLives;

    public float playerHealth;
    public float playerMaxHealth;
    public float aiHealth;
    public float aiMaxHealth;

    public Slider playerHealthSlider;

    private PlayerData pData;
    public AudioClip deathSound;
 
    //Runs before Start()
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("ERROR: There can be only ONE GAME MANAGER!");
            Destroy(gameObject);
        }

        if (aiData == null)
        {
            aiData = gameObject.GetComponent<AI_Data>();
        }

        if (pData == null)
        {
            //Debug.Log("Data has been added to the Player Motor");
            pData = gameObject.GetComponent<PlayerData>();
        }
        

    }

    private void Start()
    {
        SpawnPlayer();
        playerLives = 3;
        SpawnAI();
        aiLives = 4;  
    }

    public void SpawnPlayer()
    {
        SetPlayerHealth();
        playerDied = false;
        player = Instantiate(playerPrefab, playerSpawnLocation.position, playerSpawnLocation.rotation) as GameObject;
    }

    public void SpawnAI()
    {
        aiDied = false;
        ai = Instantiate(aiPrefab, aiSpawnLocation[0].position, aiSpawnLocation[0].rotation) as GameObject;
        ai = Instantiate(aiPrefab, aiSpawnLocation[1].position, aiSpawnLocation[1].rotation) as GameObject;
        ai = Instantiate(aiPrefab, aiSpawnLocation[2].position, aiSpawnLocation[2].rotation) as GameObject;
        ai = Instantiate(aiPrefab, aiSpawnLocation[3].position, aiSpawnLocation[3].rotation) as GameObject;
        SetAIHealth();
    }

    public void PlayerDeathAndRespawn()
    {
        if (playerDied)
        {
            AudioSource.PlayClipAtPoint(deathSound, transform.position, 1.0f);
            playerLives--;
            ResetPlayer();
        }
        if (playerLives <= 0)
        {
            SceneManager.LoadScene("Gameover");
        }
    }

    public void AIDeathAndRespawn()
    {
        if (aiDied)
        {
            AudioSource.PlayClipAtPoint(deathSound, transform.position, 1.0f);
            aiLives--;
            ResetAI();
        }
        if (aiLives <= 0)
        {
            WinCondition();
        }
    }



    private void ResetPlayer()
    {
        player.transform.position = playerSpawnLocation.position;
        SetPlayerHealth();
        playerDied = false;
    }

    private void ResetAI()
    {
        ai.transform.position = aiSpawnLocation[0].position;
        ai.transform.position = aiSpawnLocation[1].position;
        ai.transform.position = aiSpawnLocation[2].position;
        ai.transform.position = aiSpawnLocation[3].position;
        SetAIHealth();
        aiDied = false;
    }

    private void Update()
    {
        PlayerDeathAndRespawn();
        AIDeathAndRespawn();
        
        
    }

    private void SetPlayerHealth()
    {
        playerHealth = playerMaxHealth;
        playerHealthSlider.maxValue = playerHealth;
        playerHealthSlider.value = playerHealth;
    }

    private void SetAIHealth()
    {
        aiMaxHealth = 500.0f;  
        aiHealth = aiMaxHealth;
    }

    public void WinCondition()
    {
        if (playerWinTrigger && aiLives <= 0)
        {
            SceneManager.LoadScene("GameOver");
            Debug.Log("Player Wins, and is moving to the win screen.");
        }
    }

    public void RemovePlayerHealth(float amount)
    {
        playerHealth -= amount;
        playerHealthSlider.value -= amount;
        if (playerHealth <= 0)
        {
            playerDied = true;
        }
    }

}
