﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class ScoreManager : MonoBehaviour {

    public int kills = 0;
    public int deaths = 0;

    public int kKills = 0;
    public int kDeaths = 0;

    public int dKills = 0;
    public int dDeaths = 0;

    Dictionary<string, Dictionary<string, int>> playerScores;

    int changeCounter = 0;


    // Use this for initialization
    void Start() {
        SetScore("Hunter", "Kills", kills);
        SetScore("Hunter", "Deaths", deaths);
        SetScore("Hunter", "Score", (kills - deaths) * 3);

        SetScore("Kris", "Kills", kKills);
        SetScore("Kris", "Deaths", kDeaths);
        SetScore("Kris", "Score", (kKills - kDeaths) * 3);

        SetScore("Danielle", "Kills", dKills);
        SetScore("Danielle", "Deaths", dDeaths);
        SetScore("Danielle", "Score", (dKills - dDeaths) * 3);

        Debug.Log(GetScore("Hunter", "Number of Kills"));
    }


    public void Initalize()
    {
        if (playerScores != null)
        {
            return;
        }
        playerScores = new Dictionary<string, Dictionary<string, int>>();
    }

    public int GetScore(string username, string scoreType)
    {
        Initalize();
        changeCounter++;
        if (playerScores.ContainsKey(username) == false)
        {
            //We have no score record for this username.
            return 0;
        }

        if (playerScores[username].ContainsKey(scoreType) == false)
        {
            //We have no record for this score type.
            return 0;
        }
        //Returns a value with Username and ScoreType
        return playerScores[username][scoreType];
    }

    public void SetScore(string username, string scoreType, int value)
    {
        Initalize();

        if (playerScores.ContainsKey(username) == false)
        {
            playerScores[username] = new Dictionary<string, int>();
        }

        playerScores[username][scoreType] = value;
    }

    public void ChangeScore(string username, string scoreType, int amount)
    {
        Initalize();

        int currentScore = GetScore(username, scoreType);
        SetScore(username, scoreType, currentScore + amount);

    }


    public string[] GetPlayerNames()
    {
        Initalize();
        return playerScores.Keys.ToArray();
    }


    public string[] GetPlayerNames(string sortScoreType)
    {
        Initalize();

        return playerScores.Keys.OrderByDescending(n => GetScore(n, sortScoreType)).ToArray();

    }

    public void ButtonAddKillsHunter()
    {
        ChangeScore("Hunter", "Kills", 1);
    }
    public void ButtonAddKillsKris()
    {
        ChangeScore("Kris", "Kills", 1);
    }

    public void ButtonAddKillsDanielle()
    {
        ChangeScore("Danielle", "Kills", 1);
    }

    public void ButtonAddDeathsHunter()
    {
        ChangeScore("Hunter", "Deaths", 1);
    }
    public void ButtonAddDeathsKris()
    {
        ChangeScore("Kris", "Deaths", 1);
    }

    public void ButtonAddDeathsDanielle()
    {
        ChangeScore("Danielle", "Deaths", 1);
    }




    public int GetChangeCounter()
    {
        return changeCounter;
    }

}
