﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour {

    public float aiHealth;
    public float aiMaxHealth = 100.0f;
    public float playerHealth;

    public GameObject ai;
    public GameObject player;



    public void RemoveAiHealth(float amount)
    {
        aiHealth -= amount;
        if (aiHealth <= 0)
        {
            Destroy(ai);
        }
    }

    public void RemovePlayerHealth(float amount)
    {
        playerHealth -= amount;
        if (playerHealth <= 0)
        {
            Destroy(player);
        }
    }

}
