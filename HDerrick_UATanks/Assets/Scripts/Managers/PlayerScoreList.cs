﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreList : MonoBehaviour {

    public GameObject playerScoreEntryPrefab;
    private ScoreManager scoreManager;

    int lastChangeCounter;

	// Use this for initialization
	void Start () {
        scoreManager = GameObject.FindObjectOfType<ScoreManager>();
        if (scoreManager == null)
        {
            Debug.LogError("You forgot to add the score manager.");
            return;
        }

        lastChangeCounter = scoreManager.GetChangeCounter();

        scoreManager.ChangeScore("Hunter", "Kills", 1);
        scoreManager.ChangeScore("Kris", "Kills", 1);
        scoreManager.ChangeScore("Danielle", "Kills", 1);
        scoreManager.ChangeScore("Hunter", "Deaths", 1);
        scoreManager.ChangeScore("Kris", "Deaths", 1);
        scoreManager.ChangeScore("Danielle", "Deaths", 1);

    }
	
	// Update is called once per frame
	void Update () {

        if (scoreManager.GetChangeCounter() == lastChangeCounter)
        {
            //No Change since update.
        }

        lastChangeCounter = scoreManager.GetChangeCounter();

        //scoreManager.ChangeScore("Hunter", "Kills", 1);
        while (this.transform.childCount > 0)
        {
            Transform child = this.transform.GetChild(0);
            child.SetParent(null); // No longer has a parent
            Destroy(child.gameObject);
        }

        string[] killNames = scoreManager.GetPlayerNames("Kills");
        string[] deathNames = scoreManager.GetPlayerNames("Deaths");

        foreach (string name in killNames)
        {
            GameObject go = (GameObject)Instantiate(playerScoreEntryPrefab);
            go.transform.SetParent(this.transform);
            go.transform.Find("Username").GetComponent<Text>().text = name;
            go.transform.Find("Kills").GetComponent<Text>().text = scoreManager.GetScore(name, "Kills").ToString();
            //go.transform.Find("Deaths").GetComponent<Text>().text = scoreManager.GetScore(name, "Deaths").ToString();
            //go.transform.Find("Score").GetComponent<Text>().text = scoreManager.GetScore(name, "Score").ToString();
        }
        foreach (string name in deathNames)
        {
            GameObject go = (GameObject)Instantiate(playerScoreEntryPrefab);
            go.transform.SetParent(this.transform);
            go.transform.Find("Username").GetComponent<Text>().text = name;
            //go.transform.Find("Kills").GetComponent<Text>().text = scoreManager.GetScore(name, "Kills").ToString();
            go.transform.Find("Deaths").GetComponent<Text>().text = scoreManager.GetScore(name, "Deaths").ToString();
            //go.transform.Find("Score").GetComponent<Text>().text = scoreManager.GetScore(name, "Score").ToString();
        }

    }
}
