﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatController : MonoBehaviour
{

    private PowerupController powerupController;
    public Powerup cheatingPowerups;


    // Use this for initialization
    void Start()
    {
        if (powerupController == null)
        {
            //Debug.Log("The PowerupController has been added into the Cheat Controller");
            powerupController = gameObject.GetComponent<PowerupController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        HueCheatCode();
    }

    public void HueCheatCode()
    {
        //If H and U are being held down and this is the first frame the "e" is pressed...do something.
        if (Input.GetKey(KeyCode.H) && Input.GetKey(KeyCode.U) && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("So, this is what cheating feels like?");
            //Add a powerup to our tank.
            powerupController.Add(cheatingPowerups);
        }
    }
}
