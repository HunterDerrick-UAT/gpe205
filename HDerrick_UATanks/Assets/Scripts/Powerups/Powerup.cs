﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup
{

    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float missileForceModifier;

    public float duration;

    public bool isPermanent;

    public void OnActivate(PlayerData target)
    {
        target.MoveSpeed += speedModifier;
        HealthPickup();
        MaxHealthPickup();
        target.missileForce += missileForceModifier;
    }

    public void OnDeactivate(PlayerData target)
    {
        target.MoveSpeed -= speedModifier;
        HealthPickup();
        MaxHealthPickupEnd();
        target.missileForce -= missileForceModifier;
    }

    public void HealthPickup()
    {
        GameManager.instance.playerHealth += healthModifier;
        GameManager.instance.playerHealthSlider.value += healthModifier;
    }

    public void MaxHealthPickup()
    {
        GameManager.instance.playerMaxHealth += maxHealthModifier;
        GameManager.instance.playerHealthSlider.maxValue += maxHealthModifier;
    }

    public void HealthPickupEnd()
    {
        GameManager.instance.playerHealth -= healthModifier;
        GameManager.instance.playerHealthSlider.value -= healthModifier;
    }

    public void MaxHealthPickupEnd()
    {
        GameManager.instance.playerMaxHealth -= maxHealthModifier;
        GameManager.instance.playerHealthSlider.maxValue -= maxHealthModifier;
    }
}
