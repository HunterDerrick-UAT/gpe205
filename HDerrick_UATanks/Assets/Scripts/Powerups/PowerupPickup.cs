﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupPickup : MonoBehaviour
{

    public Powerup powerup;
    public AudioClip pickupAudio;


    public void OnTriggerEnter(Collider other)
    {
        PowerupController powerController = other.GetComponent<PowerupController>();

        //If our "other" has the powerController
        if (powerController != null)
        {
            //Add the powerup
            powerController.Add(powerup);

            //Play Audio for pickup.
            if (pickupAudio != null)
            {
                AudioSource.PlayClipAtPoint(pickupAudio, transform.position, 1.0f);
            }
            //Destory this powerup pickup.
            Destroy(gameObject);
        }
    }


}
