﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{

    private PlayerData pData;
    public List<Powerup> powerups;


    // Use this for initialization
    void Start()
    {
        powerups = new List<Powerup>();
        if (pData == null)
        {
            //Debug.Log("Player Data has been added to the PowerupController.");
            pData = gameObject.GetComponent<PlayerData>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckForPowerUps();
    }


    public void Add(Powerup powerup)
    {
        //Run Powerup OnActivate function of powerup.
        powerup.OnActivate(pData);

        //Only add the permanent ones to a list.
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }

    public void CheckForPowerUps()
    {
        //Create a list to hold our expired powerups.
        List<Powerup> expiredPowerups = new List<Powerup>();

        //Loop through all of the powerups in the Powerups list.
        foreach (Powerup power in powerups)
        {
            //Subtract from the timer set for the powerups.
            power.duration -= Time.deltaTime;

            //Assemble a list of all expired powerups.
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }

        //Now that we have looked through every powerup in our list, we use our expired power up list to remove the expired ones.
        foreach (Powerup power in expiredPowerups)
        {
            power.OnDeactivate(pData);
            powerups.Remove(power);
        }

        //Lets be sure to clear our expired powerup list so it isn't storing un needed information/data.
        expiredPowerups.Clear();

    }


}
