﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawner : MonoBehaviour {

    public GameObject powerupPrefab;
    public float spawnDelay;
    private float nextSpawnTime;
    public Transform spawnLocation;
    public bool isSpawned;
   


	// Use this for initialization
	void Start () {
        isSpawned = false;
        nextSpawnTime = Time.time + spawnDelay;
	}
	
	// Update is called once per frame
	void Update () {
        SpawnPowerup();
        
    }

    public void SpawnPowerup()
    {
        //TODO: Make this function if player picks up then set timer to respawn.
        if (!isSpawned)
        {
            if (Time.time > nextSpawnTime)
            {
                Instantiate(powerupPrefab, spawnLocation.position, spawnLocation.rotation);
                isSpawned = true;
                nextSpawnTime = Time.time + spawnDelay;
            }

        }
        else
        {
            nextSpawnTime = Time.time + spawnDelay;
        }
    }
}
