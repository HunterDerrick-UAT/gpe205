﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Data : MonoBehaviour {

    public float fireRate;
    public float aiHealth;
    public float aiMaxHealth;
    public GameObject ai;

    [Range(5, 15)]
    public float aiMoveSpeed;  //In meters per second
    [Range(70, 220)]
    public float aiRotateSpeed; //In degrees per second


    public void RemoveAiHealth(float amount)
    {
        GameManager.instance.aiHealth -= amount;
        if (GameManager.instance.aiHealth <= 0)
        {
            GameManager.instance.aiDied = true;
        }
    }
}
