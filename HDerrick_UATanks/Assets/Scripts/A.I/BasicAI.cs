﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAI : MonoBehaviour {

    public float speed;

    private Transform target;

    public GameObject missileSpawn;  //GameObject used as placeholder where the Missile will spawn from. Drag from Heirarchy into inspector
    public GameObject missile;       //Used as placeholder for Missile. Drag from Prefabs, into inspector

    [Range(100.0f, 5000.0f)]         //Establishing a range amount that can be adjusted for missileForce.
    public float missileForce;       //Float to adjust the amount of force of which the missle fires at. Adjust in the inspector.

    [Range(0.5f, 5.0f)]              //Establishing a range amount for destroying the missile.
    public float destroyMissile;     //Timer set to destroy missile.

    
    public float coolDown;           //Establish a set Cool Down. This will allow the player to shoot, only once the cool down reset. i.e: 1 or 2
    public float coolDownTimer;      //Making a float to perform a calculation to decrament time for the cool down.


    // Use this for initialization
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        FindPlayer();
        ShotCoolDown();
        if (coolDownTimer == 0)
        {
            Debug.Log("Enemy Fired, not facing anything.");            
            ShootPlayer();
            coolDownTimer = coolDown;
        }

    }

    void FindPlayer()
    {
        gameObject.transform.LookAt(target);
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }

    public void ShootPlayer()
    {
        //Here we instantiate our missile
        GameObject temporaryMissile;
        temporaryMissile = Instantiate(missile, missileSpawn.transform.position, missileSpawn.transform.rotation) as GameObject;

        //Retreive our Rigidbody
        Rigidbody rb;
        rb = temporaryMissile.GetComponent<Rigidbody>();

        //Using the Rigidbody component, we will "AddForce" to our missile to move it forward.
        rb.AddForce(transform.forward * missileForce);

        //Destroy Missile after seconds. Designated in inspector.
        Destroy(temporaryMissile, destroyMissile);
    }

    public void ShotCoolDown()
    {
        //If our CooldownTimer is greater than 0. Than we are subtracting time in this case seconds.
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        //Make sure that our Cool Down Timer never goes any lower than 0.
        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }

    }
}
