﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_States : MonoBehaviour {

    public enum AIState { Chase, ChaseAndFire, CheckForAvoidance, Avoidance, Rest };
    public AIState aiState = AIState.Chase;
    public float stateEnterTime;
    public float aiSenseRadius;
    public float restingHealRate;  //Healing in HP per second.

    private int avoidanceStage = 0;
    public float avoidanceTime;
    private float exitTime;

    private Transform target;
    private Transform tf;
    private Test_Motor tMotor;
    private AI_Data aiData;
    //private HealthManager hManager;

    public GameObject missileSpawn;  //GameObject used as placeholder where the Missile will spawn from. Drag from Heirarchy into inspector
    public GameObject missile;       //Used as placeholder for Missile. Drag from Prefabs, into inspector

    [Range(100.0f, 5000.0f)]         //Establishing a range amount that can be adjusted for missileForce.
    public float missileForce;       //Float to adjust the amount of force of which the missle fires at. Adjust in the inspector.

    [Range(0.5f, 5.0f)]              //Establishing a range amount for destroying the missile.
    public float destroyMissile;     //Timer set to destroy missile.

    [Range(10.0f, 50.0f)]
    public float missileDamage;

    public float coolDown;           //Establish a set Cool Down. This will allow the player to shoot, only once the cool down reset. i.e: 1 or 2
    public float coolDownTimer;      //Making a float to perform a calculation to decrament time for the cool down.

    // Use this for initialization
    void Start()
    {
        //TODO: Make this a little more fool proof.
        tf = gameObject.GetComponent<Transform>(); //Attaches the transform of GameObject script is attached to.
        tMotor = gameObject.GetComponent<Test_Motor>();  //Attaches the Test_Motor script in the Hierarchy
        aiData = gameObject.GetComponent<AI_Data>();  //Attaches the Test_Data script in the Hierarchy
        //hManager = gameObject.GetComponent<HealthManager>(); // Attaches the HealthManager script in the Hierarchy.
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>(); //Attaches the GameObject of the target. In this case the player in the Hierarchy.
    }

    // Update is called once per frame
    void Update()
    {
        if (aiState == AIState.Chase)
        {
            //Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
            }
            //Check for a transition
            if (aiData.aiHealth < aiData.aiMaxHealth * .5f)
            {
                ChangeState(AIState.CheckForAvoidance);
            }
            else if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                ChangeState(AIState.ChaseAndFire);
            }
        }

        else if (aiState == AIState.ChaseAndFire)
        {
            //Perform Behaviors.
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChaseAndFire();
            }

            //Check for a Transition.
            if (aiData.aiHealth < aiData.aiMaxHealth * .5f)
            {
                ChangeState(AIState.CheckForAvoidance);
            }
            else if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                ChangeState(AIState.Chase);
            }
        }

        else if (aiState == AIState.Avoidance)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                //TODO: Make an actual DoFlee function.
                //DoFlee();
                avoidanceStage = 1;
            }
            //Check for a transition.
            if (Time.time >= stateEnterTime + 30)
            {
                ChangeState(AIState.CheckForAvoidance);
            }
        }

        else if (aiState == AIState.CheckForAvoidance)
        {
            //Perform Behaviors
            DoCheckForAvoidance();

            //Check for a transition.
            if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                ChangeState(AIState.Avoidance);
            }
            else
            {
                ChangeState(AIState.Rest);
            }
        }

        else if (aiState == AIState.Rest)
        {
            //Perform behaviors
            DoRest();

            //Check for a transition.
            if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                ChangeState(AIState.Avoidance);
            }
            else if (aiData.aiHealth >= aiData.aiMaxHealth)
            {
                ChangeState(AIState.Chase);
            }
        }
    }

    public void DoCheckForAvoidance()
    {
        //TODO: Figure out how this function is actually supposed to work. Just kinda guessing here. 

        //If current health is half or less
        if (aiData.aiHealth <= aiData.aiMaxHealth * .5)
        {
            DoAvoidance();
        }

    }

    public void ChangeState(AIState newState)
    {

        //Change our State
        aiState = newState;

        //Save the time we changed states
        stateEnterTime = Time.time;
    }

    public void DoRest()
    {

        //Increase our health. The increase is HP per second.
        aiData.aiHealth += restingHealRate * Time.deltaTime;

        //DO NOT GO OVER MAX HEALTH
        aiData.aiHealth = Mathf.Min(aiData.aiHealth, aiData.aiMaxHealth);

        //TODO: Add in a timer for health regeneration. 
    }

    public void DoChase()
    {
        //TODO: Make this cleaner
        tMotor.RotateTowards(target.position, aiData.aiRotateSpeed);
        //Check if we can move "tData.TestMoveSpeed" units away.
        //We are choosing this distance because that is how far we move in "1 second".
        //This means we are looking for collision. "1 Second in the future."
        if (CanMove(aiData.aiMoveSpeed))
        {
            tMotor.TestMove(aiData.aiMoveSpeed);
        }
        else
        {
            //Enter into Obstacle Avoidance Stage #1
            avoidanceStage = 1;
        }
    }

    public void DoAvoidance()
    {
        //TODO: Make this
        if (avoidanceStage == 1)
        {
            //Rotate to the left
            tMotor.TestRotate(-1 * aiData.aiRotateSpeed);
            //If I can now move forward, than move to stage 2.
            if (CanMove(aiData.aiMoveSpeed))
            {
                avoidanceStage = 2;

                //Set number of seconds we will be in the avoidance stage.
                exitTime = avoidanceTime;
            }
            //Otherwise we will do this again next turn.
        }
        else if (avoidanceStage == 2)
        {
            //If we can move forward, do so. 
            if (CanMove(aiData.aiMoveSpeed))
            {
                //Subtract from our timer and move.
                exitTime -= Time.deltaTime;
                tMotor.TestMove(aiData.aiMoveSpeed);

                //If we have moved long enough, return to chase mode.
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                //Otherwise we can not move forward, so return to avoidanceStage 1
                avoidanceStage = 1;
            }
        }
    }

    public void DoChaseAndFire()
    {
        //TODO: Make this cleaner
        tMotor.RotateTowards(target.position, aiData.aiRotateSpeed);
        //Check if we can move "tData.TestMoveSpeed" units away.
        //We are choosing this distance because that is how far we move in "1 second".
        //This means we are looking for collision. "1 Second in the future."
        if (CanMove(aiData.aiMoveSpeed))
        {
            tMotor.TestMove(aiData.aiMoveSpeed);
            ShootingCooldown();
            if (coolDownTimer == 0)
            {
                Debug.Log("Enemy Fired, not facing anything.");
                //RaycastHit hit;
                //Ray ray = new Ray(transform.position, transform.forward);
                //if (Physics.Raycast(ray, out hit, 100f))
                //{
                //    //TODO: Fix the raycast to player for more accurate damage.
                //    if (hit.collider.gameObject.CompareTag("Player"))
                //    {
                //        Debug.DrawRay(transform.position, transform.forward, Color.red);
                //        Debug.Log("Enemy Fired, Raycast hit " + gameObject.name);
                //        hit.transform.GetComponent<GameManager>().RemovePlayerHealth(missileDamage);
                //    }
                //}
                ShootTarget();
                coolDownTimer = coolDown;
            }


        }
        else
        {
            //Enter into Obstacle Avoidance Stage #1
            avoidanceStage = 1;
        }
    }

    public void DoFlee()
    {
        //TODO: Make this.
    }

    bool CanMove(float speed)
    {
        //TODO: Make this. This will check if we can move forward by "Speed" units.

        //Cast a Ray forward in the distance/direction we sent in.
        //If our raycast hit something
        RaycastHit hit;
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            //If what we hit IS NOT THE player...
            if (!hit.collider.CompareTag("Player"))
            {
                //Then we can not move.
                return false;
            }
        }
        //Otherwise, return true.
        return true;

    }

    public void ShootTarget()
    {
        //Here we instantiate our missile
        GameObject temporaryMissile;
        temporaryMissile = Instantiate(missile, missileSpawn.transform.position, missileSpawn.transform.rotation) as GameObject;

        //Retreive our Rigidbody
        Rigidbody rb;
        rb = temporaryMissile.GetComponent<Rigidbody>();

        //Using the Rigidbody component, we will "AddForce" to our missile to move it forward.
        rb.AddForce(transform.forward * missileForce);

        //Destroy Missile after seconds. Designated in inspector.
        Destroy(temporaryMissile, destroyMissile);
    }


    public void ShootingCooldown()
    {
        //If our CooldownTimer is greater than 0. Than we are subtracting time in this case seconds.
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        //Make sure that our Cool Down Timer never goes any lower than 0.
        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }
    }

}
