﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMissile : MonoBehaviour {

    public float aiMissileDamage;


    private void OnCollisionEnter(Collision missilePrefab)
    {
        if (missilePrefab.gameObject.CompareTag("Player"))
        {
            GameManager.instance.RemovePlayerHealth(aiMissileDamage);
            Destroy(gameObject);
        }
    }
}
