﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMissile : MonoBehaviour {

    public float playerMissileDamage;

    private void OnCollisionEnter(Collision missilePrefab)
    {
        if (missilePrefab.gameObject.CompareTag("Enemy"))
        {
            missilePrefab.gameObject.GetComponent<AI_Data>().RemoveAiHealth(playerMissileDamage);
        }        
    }

}
