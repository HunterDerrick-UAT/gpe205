﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMissile : MonoBehaviour {

    public bool enemyHit;
    public bool playerHit;

    [Range(10, 100)]
    public float enemyMissileDamage;    //Allows designers to designate how much damage an Enemy Missile does.
    [Range(10, 100)]
    public float playerMissileDamage;   //Allows designers to designate how much damage a Player Missile does.


    // Use this for initialization
    void Start()
    {
        enemyHit = false;
        playerHit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHit)
        {
            Destroy(GameObject.FindGameObjectWithTag("Enemy"));
        }
        if (playerHit)
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("Destroy Enemy");
            enemyHit = true;
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Player Hit");
            playerHit = true;
        }

    }
}
