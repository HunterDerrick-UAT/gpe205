﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnableMenu : MonoBehaviour {

    public GameObject pauseMenu;

    // Update is called once per frame
    void Update () {
        PauseMenu();
	}


    public void PauseMenu()
    {
        if (Input.GetButtonDown("Pause"))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf);
        }
    }
}
