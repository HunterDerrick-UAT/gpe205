﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour {

    public AudioClip buttonPress;

    public void OnePlayer()
    {
        AudioSource.PlayClipAtPoint(buttonPress, transform.position, 1.0f);
        SceneManager.LoadScene("Main_00");
    }

    public void TwoPlayer()
    {
        AudioSource.PlayClipAtPoint(buttonPress, transform.position, 1.0f);
        SceneManager.LoadScene("Main_TwoPlayer");
    }

    public void MOTD()
    {
        AudioSource.PlayClipAtPoint(buttonPress, transform.position, 1.0f);
        SceneManager.LoadScene("MapGen");
    }

    public void QuitGame()
    {
        AudioSource.PlayClipAtPoint(buttonPress, transform.position, 1.0f);
        Debug.Log("Game Quitting");
        Application.Quit();
    }

}
