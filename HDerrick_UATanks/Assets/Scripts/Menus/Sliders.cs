﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sliders : MonoBehaviour {

    public Slider volumeSlider;

    public AudioSource volumeAudio;

    private void Start()
    {
        volumeSlider.value = volumeAudio.volume;
    }

    public void ChangeVolume()
    {
        volumeAudio.volume = volumeSlider.value;
    }

  


}
